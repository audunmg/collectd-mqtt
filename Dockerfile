FROM docker.io/library/alpine:3.15

RUN apk add collectd collectd-mqtt collectd-network

ENTRYPOINT [ "/usr/sbin/collectd", "-f" ]
